<?php

namespace frontend\controllers;


use frontend\models\Test;
use frontend\models\Validate;
use yii\web\Controller;
use Yii;

class TestController extends Controller
{
    public function actionIndex()
    {
        $limit = \Yii::$app->params['maxNewsInList'];
        $list = Test::getNewsList($limit);

        return $this->render('index', [
            'list' => $list,
        ]);
    }

    public function actionView($id)
    {
        $item = Test::getItem($id);

        return $this->render('view', [
            'item' => $item
        ]);
    }


    public function actionMail()
    {
        $result = Yii::$app->mailer->compose()
            ->setFrom('rwwwalker@gmail.com')
            ->setTo('ruslan.php.dev@gmail.com')
            ->setSubject('Тема сообщения')
            ->setTextBody('Текст сообщения')
            ->setHtmlBody('<b>текст сообщения в формате HTML</b>')
            ->send();
    }

    public function actionValidate()
    {
        $model = new Validate();

        if (Yii::$app->request->isPost) {
            $model->attributes = Yii::$app->request->post();

            if ($model->validate()) {
                Yii::$app->session->setFlash('success', 'VALID!!!');
            }
        }

        return $this->render('validator', [
            'model' => $model,
        ]);
    }


}