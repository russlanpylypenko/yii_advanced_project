<?php

use yii\widgets\ActiveField;
use yii\helpers\Html;
use frontend\components\HightLightHelper;

/* @var $this yii\web\View */
?>
<h1>Полнотекстовый поиск</h1>

<?php $form = \yii\bootstrap\ActiveForm::begin(['layout' => 'inline']); ?>

<?= $form->field($model, 'keyword', [
    'inputTemplate' => '<div class="input-group">{input}<span class="input-group-btn">' .
        '<button class="btn btn-primary">Поиск!</button></span></div>',
])->hint('Введите слово или фразу для поиска'); ?>
<?php $form::end() ?>

<?php if (!empty($rusults)) foreach ($rusults as $result): ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4><?= $result['title']; ?></h4>
        </div>
        <div class="panel-body">
            <?= html_entity_decode(HightLightHelper::getHightLightText($result['text'], $model->keyword)); ?>
        </div>
    </div>


    <hr>
<?php endforeach; ?>

