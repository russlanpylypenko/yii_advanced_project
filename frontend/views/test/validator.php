<?php if ($model->hasErrors()):?>
<pre>
    <?php print_r($model->errors); ?>
</pre>
<?php endif; ?>

<form method="post">
    <div class="form-group">
        <label for="start">start:</label>
        <input type="text" class="form-control" name="start" id="start" value="<?= $model->start; ?>">
        <label for="end">end:</label>
        <input type="text" class="form-control" name="end" id="end" value="<?= $model->end; ?>">
    </div>
    <button type="submit" class="btn btn-default">Submit</button>
</form>