<?php

namespace frontend\models;

use frontend\components\StringHelper;
use Yii;

class Test
{
    public static function getNewsList($limit)
    {
        $limit = (int)$limit;
        $sql = 'SELECT * FROM news LIMIT '. $limit;

        $result = Yii::$app->db->createCommand($sql)->queryAll();

        $helper = Yii::$app->stringHelper;
        if($result) foreach ($result as &$item){
            $item['content'] = $helper->getShort($item['content']);
        }

        return $result;
    }

    public static function getItem($id)
    {
        $id = (int)$id;
        $sql = "SELECT * FROM news WHERE id = $id";

        return Yii::$app->db->createCommand($sql)->queryOne();
    }

}