<?php

namespace frontend\models;


use yii\base\Model;

class Validate extends Model
{

    public $start;
    public $end;

    public function rules()
    {
       return [
           [['start', 'end'], 'required'],
           [['start', 'end'], 'integer'],
           ['start', 'compare',
               'compareAttribute' => 'end',
               'operator' => '<',
               'message' => 'TEESST',
           ],

       ];
    }

}