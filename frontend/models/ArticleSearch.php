<?php

namespace frontend\models;

use Yii;
use yii\helpers\ArrayHelper;

class ArticleSearch
{

    public function fulltextSearch($keyword)
    {
        $sql = "SELECT * FROM articles WHERE MATCH (text) AGAINST (:keyword) LIMIT 20";
        return Yii::$app->db->createCommand($sql, [':keyword' => $keyword])->queryAll();
    }


    public function sphinxSearch($keyword)
    {
        $sql = "SELECT * FROM idx_my_text WHERE MATCH('$keyword') OPTION ranker=WORDCOUNT";
        $data = Yii::$app->sphinx->createCommand($sql)->queryAll();
        $ids = ArrayHelper::map($data, 'id', 'id');
        $data = Articles::find()->where(['id' => $ids])->asArray()->all();

        $data = ArrayHelper::index($data, 'id');
        $result = [];

        foreach ($ids as $element){
            $result[] = $data[$element];
        }

        return $result;
    }
}