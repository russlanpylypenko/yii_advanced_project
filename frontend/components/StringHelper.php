<?php

namespace frontend\components;


class StringHelper
{
    private $limit;

    public function __construct()
    {
        $this->limit = \Yii::$app->params['shortTextLimit'];
    }

    public function getShort($string, $limit = null)
    {
        $limit = $limit === null ? $this->limit : $limit;
        return substr($string, 0, $limit);
    }
}