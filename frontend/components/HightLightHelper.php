<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 16.02.19
 * Time: 14:34
 */

namespace frontend\components;


class HightLightHelper
{
    public static function getHightLightText($string, $keyword){
        return preg_replace("/$keyword/","<span class='bg-info'>$keyword</span>", $string);
    }
}