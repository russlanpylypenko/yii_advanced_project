<?php

use yii\db\Migration;

/**
 * Class m190216_120134_add_index_articles_text
 */
class m190216_120134_add_index_articles_text extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE articles ADD FULLTEXT INDEX idx_text (text)");
    }

    public function down()
    {
        $this->dropIndex('idx_text', 'articles');
    }
}
