<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%employee}}`.
 */
class m190210_132714_create_employee_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%employee}}', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string(255),
            'second_name' => $this->string(),
            'last_name' => $this->string(),
            'city' => $this->string(),
            'start_date' => $this->date(),
            'role' => $this->string(),
            'num_office'=> $this->integer(11),
            'zip_code' => $this->integer(11),
            'email' => $this->string()->unique(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%employee}}');
    }
}
