<?php

use yii\db\Migration;

/**
 * Handles adding salary to table `{{%employee}}`.
 */
class m190210_133359_add_salary_column_to_employee_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%employee}}', 'salary', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%employee}}', 'salary');
    }
}
