<?php

use yii\db\Migration;

/**
 * Handles dropping city from table `{{%employee}}`.
 */
class m190210_133531_drop_city_column_from_employee_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('employee', 'city');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('employee', 'city', $this->integer());
    }
}
