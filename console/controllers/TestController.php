<?php
namespace console\controllers;


use yii\console\Controller;
use yii\helpers\Console;

class TestController extends Controller
{
    public $name;
    public $age;
    public $mark;


    public function options($actionID)
    {
        return ['name', 'age', 'mark'];
    }


    public function actionTest()
    {
        $file = '/home/reston/images.com/log.txt';
        $file = fopen($file, 'a+');
        fwrite($file, date("Y-m-d H:i:s") . "\r\n");
        $name = $this->ansiFormat('Alex', Console::FG_YELLOW);
        echo "Hello, my name is $name. \r\n";
    }

    public function actionGetStudent()
    {
        $name = $this->name ? $this->name : "John";
        $age = $this->age ? $this->age : "21";
        $mark = $this->mark ? $this->mark : rand(60, 95);

        $name = $this->ansiFormat($name, Console::FG_BLUE);
        $age = $this->ansiFormat($age, Console::FG_RED);
        $mark = $this->ansiFormat($mark, Console::BOLD);

        echo "I`am $name, my age $age i has mark -> $mark \r\n";
    }

}